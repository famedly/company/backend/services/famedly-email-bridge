#!/bin/sh
if [ ! -f "/data/config.yaml" ]; then
	echo "No config found"
	exit 1
fi
if [ ! -f "/data/email-registration.yaml" ]; then
	node /opt/famedly-email-bridge/build/src/index.js -c /data/config.yaml -f /data/email-registration.yaml -r
	echo "Registration generated."
	exit 0
fi
node /opt/famedly-email-bridge/build/src/index.js -c /data/config.yaml -f /data/email-registration.yaml
