/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { expect } from "chai";
import { Bridge } from "../src/bridge";

// we are a test file and thus our linting rules are slightly different
// tslint:disable:no-unused-expression max-file-line-count no-any

function getBridge() {
	const config = {
		bridge: {
			homeserverUrl: "http://localhost",
			domain: "localhost",
		},
		email: {
			prefix: "matrix_",
			domain: "localhost",
		},
	} as any;
	const bridge = new Bridge("", config);
	bridge.appservice = {
		getUserId: (localpart) => {
			return `@${localpart}:${config.bridge.domain}`;
		},
	} as any;
	return bridge;
}

describe("Bridge", () => {
	describe("getUrlFromMxc", () => {
		it("should return the proper URL", () => {
			const bridge = getBridge();
			const ret = bridge.getUrlFromMxc("mxc://localhost/somefile");
			expect(ret).to.equal("http://localhost/_matrix/media/v1/download/localhost/somefile");
		});
	});
	describe("getEmailFromMxid", () => {
		it("should return a simple email for own mxids", () => {
			const bridge = getBridge();
			const ret = bridge.getEmailFromMxid("@sorunome:localhost");
			expect(ret).to.equal("matrix_sorunome@localhost");
		});
		it("should return complex emails for foreign mxids", () => {
			const bridge = getBridge();
			const ret = bridge.getEmailFromMxid("@sorunome:matrix.org");
			expect(ret).to.equal("matrix_^=40sorunome=3amatrix=2eorg@localhost");
		});
		it("should escape complex emails properly", () => {
			const bridge = getBridge();
			const ret = bridge.getEmailFromMxid("@soru#nome:matrix.org");
			expect(ret).to.equal("matrix_^=40soru=23nome=3amatrix=2eorg@localhost");
		});
	});
	describe("getMxidFromEmail", () => {
		it("should return null if improperly prefixed", () => {
			const bridge = getBridge();
			const ret = bridge.getMxidFromEmail("sorunome@localhost");
			expect(ret).to.be.null;
		});
		it("should get the mxid of simple, own emails", () => {
			const bridge = getBridge();
			const ret = bridge.getMxidFromEmail("matrix_sorunome@localhost");
			expect(ret).to.equal("@sorunome:localhost");
		});
		it("should get the mxid of complex, foreign emails", () => {
			const bridge = getBridge();
			const ret = bridge.getMxidFromEmail("matrix_^=40sorunome=3amatrix=2eorg@localhost");
			expect(ret).to.equal("@sorunome:matrix.org");
		});
		it("should unescape complex emails properly", () => {
			const bridge = getBridge();
			const ret = bridge.getMxidFromEmail("matrix_^=40soru=23nome=3amatrix=2eorg@localhost");
			expect(ret).to.equal("@soru#nome:matrix.org");
		});
	});
});
