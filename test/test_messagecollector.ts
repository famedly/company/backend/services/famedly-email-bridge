/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { expect } from "chai";
import { MessageCollector } from "../src/messagecollector";

// we are a test file and thus our linting rules are slightly different
// tslint:disable:no-unused-expression max-file-line-count no-any

const HALFSECOND = 500;

describe("MessageCollector", () => {
	describe("isCollecting", () => {
		it("should return false if it isn't collecting", () => {
			const mc = new MessageCollector(HALFSECOND);
			const ret = mc.isCollecting("!room:localhost", "@user:localhost");
			expect(ret).to.be.false;
		});
		it("should return true if it is collecting", async () => {
			const mc = new MessageCollector(HALFSECOND);
			await mc.start("!room:localhost", "@user:localhost");
			const ret = mc.isCollecting("!room:localhost", "@user:localhost");
			expect(ret).to.be.true;
		});
		it("should return false if someone else is collecting", async () => {
			const mc = new MessageCollector(HALFSECOND);
			await mc.start("!room:localhost", "@user:localhost");
			const ret = mc.isCollecting("!room:localhost", "@user2:localhost");
			expect(ret).to.be.false;
		});
	});
	describe("wait", () => {
		it("should wait the desired time", async () => {
			const mc = new MessageCollector(HALFSECOND);
			const before = new Date().getTime();
			await mc.start("!room:localhost", "@user:localhost");
			await mc.wait("!room:localhost", "@user:localhost");
			const now = new Date().getTime();
			expect(now - before).to.be.at.least(HALFSECOND);
		});
		it("should stop beforehand if a stop is called", async () => {
			const mc = new MessageCollector(HALFSECOND);
			const before = new Date().getTime();
			await mc.start("!room:localhost", "@user:localhost");
			const promise = mc.wait("!room:localhost", "@user:localhost");
			mc.stop("!room:localhost", "@user:localhost");
			await promise;
			const now = new Date().getTime();
			expect(now - before).to.be.below(HALFSECOND);
		});
		it("should stop beforehand if isCOllecting is called with a different userId", async () => {
			const mc = new MessageCollector(HALFSECOND);
			const before = new Date().getTime();
			await mc.start("!room:localhost", "@user:localhost");
			const promise = mc.wait("!room:localhost", "@user:localhost");
			mc.isCollecting("!room:localhost", "@user2:localhost");
			await promise;
			const now = new Date().getTime();
			expect(now - before).to.be.below(HALFSECOND);
		});
	});
	describe("collect", () => {
		it("should collect events", async () => {
			const mc = new MessageCollector(HALFSECOND);
			await mc.start("!room:localhost", "@user:localhost");
			const promise = mc.wait("!room:localhost", "@user:localhost");
			mc.collect("!room:localhost", "@user:localhost", "event1" as any);
			mc.collect("!room:localhost", "@user:localhost", "event2" as any);
			const ret = await promise;
			const COLLECT_LENGTH = 2;
			expect(ret.length).to.equal(COLLECT_LENGTH); expect(ret[0]).to.equal("event1");
			expect(ret[1]).to.equal("event2");
		});
		it("should ignore events not sent to our user", async () => {
			const mc = new MessageCollector(HALFSECOND);
			await mc.start("!room:localhost", "@user:localhost");
			const promise = mc.wait("!room:localhost", "@user:localhost");
			mc.collect("!room:localhost", "@user:localhost", "event1" as any);
			mc.collect("!room:localhost", "@user2:localhost", "event2" as any);
			const ret = await promise;
			expect(ret.length).to.equal(1);
			expect(ret[0]).to.equal("event1");
		});
	});
});
