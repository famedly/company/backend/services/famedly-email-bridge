/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { expect } from "chai";
import { EmailParser } from "../src/emailparser";
import * as fs from "fs";

// we are a test file and thus our linting rules are slightly different
// tslint:disable:no-unused-expression max-file-line-count no-any

describe("EmailParser", () => {
	describe("parse", () => {
		it("should parse plain emails", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/plain", "utf8");
			const parsed = ep.parse(email);
			const NUM_NODES = 1;
			expect(parsed.nodes.length).to.equal(NUM_NODES);
			expect(parsed.nodes[0].type).to.equal("text");
			expect(parsed.nodes[0].msgtype).to.equal("m.text");
			expect(parsed.nodes[0].body).to.equal("sdfjasdkl");
			expect(parsed.nodes[0].formattedBody).to.not.be.ok;
		});
		it("should parse from and to from emails", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/plain", "utf8");
			const parsed = ep.parse(email);
			expect(parsed.from.name).to.equal("Sorunome");
			expect(parsed.from.email).to.equal("mail@sorunome.de");
			expect(parsed.to.name).to.equal("famedly-email@sorunome.de");
			expect(parsed.to.email).to.equal("famedly-email@sorunome.de");
		});
		it("should parse html emails", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/html", "utf8");
			const parsed = ep.parse(email);
			const NUM_NODES = 1;
			expect(parsed.nodes.length).to.equal(NUM_NODES);
			expect(parsed.nodes[0].type).to.equal("text");
			expect(parsed.nodes[0].msgtype).to.equal("m.text");
			expect(parsed.nodes[0].body).to.equal("hey, this is *bold* and this is /italic/");
			expect(parsed.nodes[0].formattedBody).to.equal("<p>hey, this is <b>bold</b> and this is <i>italic</i><br />\r\n    </p>");
		});
		it("should strip bad html from emails", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/bad_html", "utf8");
			const parsed = ep.parse(email);
			const NUM_NODES = 1;
			expect(parsed.nodes.length).to.equal(NUM_NODES);
			expect(parsed.nodes[0].type).to.equal("text");
			expect(parsed.nodes[0].msgtype).to.equal("m.text");
			expect(parsed.nodes[0].body).to.equal("hey, this is *bold* and this is /italic/");
			expect(parsed.nodes[0].formattedBody).to.equal("<p>hey, this is <b>bold</b> and this is <i>italic</i><br />\r\n    \r\n    </p>");
		});
		it("should parse emails with a pgp signature", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/pgp-signature", "utf8");
			const parsed = ep.parse(email);
			const NUM_NODES = 2;
			const TEXT_NODE = 0;
			const PGP_NODE = 1;
			expect(parsed.nodes.length).to.equal(NUM_NODES);
			expect(parsed.nodes[TEXT_NODE].type).to.equal("text");
			expect(parsed.nodes[PGP_NODE].type).to.equal("file");
			expect(parsed.nodes[PGP_NODE].msgtype).to.equal("m.file");
			expect(parsed.nodes[PGP_NODE].info!.filename).to.equal("signature.asc");
			expect(parsed.nodes[PGP_NODE].info!.mimetype).to.equal("application/pgp-signature");
		});
		it("should parse emails with txt attachment", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/txt-attachment", "utf8");
			const parsed = ep.parse(email);
			const NUM_NODES = 2;
			const TEXT_NODE = 0;
			const TXT_NODE = 1;
			expect(parsed.nodes.length).to.equal(NUM_NODES);
			expect(parsed.nodes[TEXT_NODE].type).to.equal("text");
			expect(parsed.nodes[TXT_NODE].type).to.equal("file");
			expect(parsed.nodes[TXT_NODE].msgtype).to.equal("m.file");
			expect(parsed.nodes[TXT_NODE].info!.filename).to.equal("test.txt");
			expect(parsed.nodes[TXT_NODE].info!.mimetype).to.equal("text/plain");
		});
		it("should parse emails with png attachment", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/png-attachment", "utf8");
			const parsed = ep.parse(email);
			const NUM_NODES = 2;
			const TEXT_NODE = 0;
			const PNG_NODE = 1;
			expect(parsed.nodes.length).to.equal(NUM_NODES);
			expect(parsed.nodes[TEXT_NODE].type).to.equal("text");
			expect(parsed.nodes[PNG_NODE].type).to.equal("file");
			expect(parsed.nodes[PNG_NODE].msgtype).to.equal("m.image");
			expect(parsed.nodes[PNG_NODE].info!.filename).to.equal("Screenshot_2019-09-11_13-42-02.png");
			expect(parsed.nodes[PNG_NODE].info!.mimetype).to.equal("image/png");
		});
		it("should parse message ID fields correctly on replies", () => {
			const ep = new EmailParser();
			const email = fs.readFileSync("test/emails/reply", "utf8");
			const parsed = ep.parse(email);
			expect(parsed.messageId).to.equal("f1e0103a-0add-f453-72d7-eb1fbe72f4b6@sorunome.de");
			expect(parsed.inReplyTo).to.equal("334c137f-1783-8094-aab5-849eaaa90282@sorunome.de");
			expect(parsed.references).to.equal("334c137f-1783-8094-aab5-849eaaa90282@sorunome.de");
		});
	});
});
