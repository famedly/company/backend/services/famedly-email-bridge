/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { EmailEmailConfig } from "./config";
import { Log } from "./log";
import { EmailParser, IEmailParseResult } from "./emailparser";
import * as imaps from "imap-simple";
import { EventEmitter } from "events";
import * as nodemailer from "nodemailer";
import * as fs from "fs";

const readFileAsync = async (filename: string): Promise<Buffer> => {
	return new Promise((res, rej) => {
		fs.readFile(filename, (err, ret) => {
			err ? rej(err) : res(ret);
		});
	});
};

const log = new Log("Email");

interface ISendEmailEmail {
	name?: string;
	email: string;
}

interface ISendEmailAttachment {
	filename: string;
	buffer: Buffer;
	contentType?: string;
}

export interface ISendEmail {
	subject: string;
	from: ISendEmailEmail;
	to: ISendEmailEmail;
	attachments?: ISendEmailAttachment[];
	body: string;
	html?: string;
	reply?: string;
}

export class Email extends EventEmitter {
	private emailParser: EmailParser;
	private imap: imaps.ImapSimple;
	private smtp: nodemailer.SMTPTransport;
	constructor(
		private config: EmailEmailConfig,
	) {
		super();
		this.emailParser = new EmailParser();
	}

	public async start(): Promise<void> {
		// create the smtp transport for sending emails
		this.smtp = nodemailer.createTransport({
			host: this.config.smtp.host,
			port: this.config.smtp.port,
			secure: this.config.smtp.tls,
			auth: {
				user: this.config.smtp.user,
				pass: this.config.smtp.password,
			},
		});

		// start listening to imap to receive emails
		this.imap = await imaps.connect({
			imap: {
				user: this.config.imap.user,
				password: this.config.imap.password,
				host: this.config.imap.host,
				port: this.config.imap.port,
				tls: this.config.imap.tls,
				authTimeout: 3000,
			},
			onmail: async (numNewMail: number) => {
				log.info(`Got ${numNewMail} new email(s), fetching them...`);
				await this.fetchMail();
			},
		});
		await this.imap.openBox(this.config.inbox);
	}

	public async fetchMail(): Promise<void> {
		log.info(`Fetching new emails from inbox ${this.config.inbox}`);
		await this.imap.openBox(this.config.inbox);
		const results = await this.imap.search(["UNSEEN"], {
			bodies: [""],
			markSeen: true,
		});
		log.info(`Got ${results.length} unread emails`);
		for (const r of results) {
			const email = this.emailParser.parse(r.parts[0].body);
			log.info(`New email from ${email.from.email} to ${email.to.email}`);
			this.emit("email", email);
		}
	}

	public async getMailByMessageId(messageId: string): Promise<IEmailParseResult | null> {
		await this.imap.openBox(this.config.inbox);
		const results = await this.imap.search([["HEADER", "Message-ID", `<${messageId}>`]], {
			bodies: [""],
			markSeen: false,
		});
		if (results.length === 0) {
			return null; // nothing found
		}
		return this.emailParser.parse(results[0].parts[0].body);
	}

	public async sendMail(email: ISendEmail): Promise<string | null> {
		// build up the basic email
		const sendEmail = {
			from: this.getEmailHeader(email.from),
			to: this.getEmailHeader(email.to),
			subject: email.subject,
			text: await this.applyTemplate({body: email.body}, "plain"),
			attachments: [],
		} as any; // tslint:disable-line no-any
		// unfortunately the message object has no type

		// add HTML content, if present
		if (email.html) {
			sendEmail.html = await this.applyTemplate({body: email.html}, "html");
		}

		// add In-Reply-To and References information, if present
		if (email.reply) {
			sendEmail.inReplyTo = `<${email.reply}>`;
		}

		// add all attachments
		if (email.attachments) {
			for (const a of email.attachments) {
				sendEmail.attachments.push({
					filename: a.filename,
					content: a.buffer,
					contentType: a.contentType,
				});
			}
		}

		// finally send the email
		const info = await this.smtp.sendMail(sendEmail);
		return this.emailParser.normalizeMessageId(info.messageId);
	}

	public async applyTemplate(replaces: {[key: string]: string}, type: string): Promise<string> {
		let content = (await readFileAsync(this.config.templates[type])).toString();
		for (const key in replaces) {
			if (replaces.hasOwnProperty(key)) {
				content = content.replace(`%${key.toUpperCase()}%`, replaces[key]);
			}
		}
		return content;
	}

	private getEmailHeader(email: ISendEmailEmail): string {
		if (email.name) {
			return `${email.name} <${email.email}>`;
		}
		return email.email;
	}
}
