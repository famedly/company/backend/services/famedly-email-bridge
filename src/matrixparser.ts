import { MatrixMessageEventType, IMatrixMessage } from "./matrixtypes";
import * as escapeHtml from "escape-html";
import { Bridge } from "./bridge";
import { ISendEmail } from "./email";
import { Util } from "./util";

export class MatrixParser {
	public constructor(
		private bridge: Bridge,
	) { }

	public async parse(events: MatrixMessageEventType[]): Promise<ISendEmail | null> {
		// let's first iterate over the array and process redactions / edits
		for (const e of events) {
			if (e.type === "m.room.message" && e.content["m.relates_to"] && e.content["m.relates_to"].rel_type === "m.replace") {
				// we have an event to edit
				const ix = events.findIndex((el) => {
					return el.event_id === e.content["m.relates_to"].event_id;
				});
				events[ix].content = e.content["m.new_content"];
				// we musn't forget to delete the fallback of our message
				e.content.body = "";
				(e.content as IMatrixMessage).formatted_body = "";
			} else if (e.type === "m.room.redaction") {
				// we have to delete an event
				const ix = events.findIndex((el) => {
					return el.event_id === e.redacts;
				});
				events.splice(ix, 1);
			}
		}

		// okay, now that we reduced our events, let's iterate over them and create a message and attachments out of it
		const result = {
			subject: "",
			from: { email: "" },
			to: { email: "" },
			body: "",
			html: "",
			attachments: [],
		} as ISendEmail;

		for (const e of events) {
			if (e.type === "m.room.message") {
				// we have a message to look at!
				switch (e.content.msgtype) {
					case "m.text":
					case "m.notice":
					case "m.emote": {
						const content = e.content as IMatrixMessage;
						// normal text message
						if (!content.body) {
							// empty body, nothing to do
							break;
						}
						let html = content.formatted_body;
						if (!html) {
							html = escapeHtml(content.body);
						}
						if (content.msgtype === "m.notice") {
							html = `<font color="#aaaaaa">${html}</font>`;
						}
						// TODO: prefix name with m.emote
						result.body += content.body + "\n\n";
						result.html += html + "<br><br>";
						break;
					}
					case "m.image":
					case "m.audio":
					case "m.video":
					case "m.file": {
						const content = e.content;
						// first download the buffer
						const mxc = content.url as string;
						const url = this.bridge.getUrlFromMxc(mxc);
						const buffer = await Util.DownloadFile(url);

						// now determine the content type
						let contentType = content.info ? content.info.mimetype : undefined;
						if (!contentType) {
							contentType = Util.GetMimeType(buffer);
						}

						// we have a file of some sorts!
						result.attachments!.push({
							filename: content.body || "matrix_file",
							buffer,
							contentType,
						});
					}
				}
			}
		}

		// now check if the email is empty
		if (!result.body && result.attachments!.length === 0) {
			return null;
		}

		return result;
	}
}
