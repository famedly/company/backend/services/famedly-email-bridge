/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { EmailConfig } from "./config";
import { Log } from "./log";
import {
	IAppserviceRegistration,
	Appservice,
	Intent,
	MatrixClient,
} from "matrix-bot-sdk";
import { Store } from "./store";
import { Email, ISendEmail } from "./email";
import { IEmailParseResult, IEmailParse } from "./emailparser";
import { Util } from "./util";
import * as escapeHtml from "escape-html";
import { MessageCollector } from "./messagecollector";
import {
	MatrixMessageEventType,
	MatrixRoomEventType,
	IMatrixRoomEventMember,
	IMatrixEventContent,
	IMatrixMessage,
} from "./matrixtypes";
import { MatrixParser } from "./matrixparser";
import * as yaml from "js-yaml";
import * as fs from "fs";
import * as uuid from "uuid/v4";

const log = new Log("Bridge");

export class Bridge {
	public appservice: Appservice;
	private store: Store;
	private email: Email;
	private messageCollector: MessageCollector;
	private matrixParser: MatrixParser;
	constructor(
		private registrationPath: string,
		private config: EmailConfig,
	) { }

	/*
	 * Initialize the bridge....basically an async constructor
	 */
	public async init(): Promise<void> {
		this.store = new Store(this.config.database);
		await this.store.init();

		this.email = new Email(this.config.email);
		this.messageCollector = new MessageCollector(this.config.messageCollectingTimeout);
		this.matrixParser = new MatrixParser(this);
	}

	/*
	 * Start the bridge
	 */
	public async start(): Promise<void> {
		log.info("Starting application service...");
		let registration: IAppserviceRegistration | null = null;
		try {
			registration = yaml.safeLoad(fs.readFileSync(this.registrationPath, "utf8")) as IAppserviceRegistration;
		} catch (err) {
			log.error("Failed to load registration file", err);
			process.exit(-1);
		}
		if (!registration) {
			log.error("Registration file seems blank");
			process.exit(-1);
		}
		this.appservice = new Appservice({
			bindAddress: this.config.bridge.bindAddress,
			homeserverName: this.config.bridge.domain,
			homeserverUrl: this.config.bridge.homeserverUrl,
			port: this.config.bridge.port,
			registration: registration as IAppserviceRegistration,
		});
		this.appservice.on("room.event", this.handleRoomEvent.bind(this));
		this.appservice.on("room.invite", this.handleInviteEvent.bind(this));
		this.appservice.on("query.room", this.handleRoomQuery.bind(this));
		await this.appservice.begin();
		this.email.on("email", this.handleIncomingEmail.bind(this));
		await this.email.start();
	}

	/*
	 * Fetches the public URL of an mxc url
	 */
	public getUrlFromMxc(mxc: string): string {
		return `${this.config.bridge.homeserverUrl}/_matrix/media/v1/download/${mxc.substring("mxc://".length)}`;
	}

	/*
	 * Fetches the email address of an mxid
	 */
	public getEmailFromMxid(mxid: string): string {
		const parts = mxid.split(":");
		let emailPart = "";
		if (parts[1] === this.config.bridge.domain) {
			emailPart = parts[0].substring(1); // remove the @
		} else {
			emailPart = "^" + Util.str2mxid(mxid);
		}
		return `${this.config.email.prefix}${emailPart}@${this.config.email.domain}`;
	}

	/*
	 * fetches the mxid from an email address
	 */
	public getMxidFromEmail(email: string): string | null {
		if (!email.startsWith(this.config.email.prefix)) {
			return null;
		}
		const parts = email.split("@");
		if (parts[1] !== this.config.email.domain) {
			return null;
		}
		const localpart = parts[0].substring(this.config.email.prefix.length);
		if (localpart[0] === "^") {
			// we have an external mxid
			const mxid = Util.mxid2str(localpart.substring(1));
			return mxid;
		}
		return this.appservice.getUserId(localpart);
	}

	/*
	 * Get an email from a ghost
	 */
	public getEmailFromGhost(ghost: string): string | null {
		const suffix = this.appservice.getSuffixForUserId(ghost);
		if (!suffix) {
			return null;
		}
		return Util.mxid2str(suffix);
	}

	/*
	 * Get a ghost from an email
	 */
	public getGhostFromEmail(email: string): string {
		return this.appservice.getUserIdForSuffix(Util.str2mxid(email));
	}

	/*
	 * Get the profile name of someones mxid
	 */
	public async getNameFromMxid(mxid: string, roomId?: string, client?: MatrixClient): Promise<string | null> {
		if (!client) {
			client = this.appservice.botIntent.underlyingClient;
		}
		if (roomId) {
			try {
				const ret = await client.getRoomStateEvent(roomId, "m.room.member", mxid);
				return ret.displayname;
			} catch (err) {
				log.info("Failed to fetch m.room.member name, falling back to profile");
			}
		}
		try {
			const ret = await client.getUserProfile(mxid);
			return ret.displayname;
		} catch (err) {
			log.warn("Failed to fetch profile", err);
		}
		return null;
	}

	/*
	 * Gets the name of a room
	 */
	public async getRoomNameFromMxid(roomId: string, client?: MatrixClient): Promise<string | null> {
		if (!client) {
			client = this.appservice.botIntent.underlyingClient;
		}
		try {
			const ret = await client.getRoomStateEvent(roomId, "m.room.name", "");
			return ret.name;
		} catch (err) {
			log.warn("Failed to fetch room name", err);
		}
		return null;
	}

	public canMxidUseBridge(mxid: string): boolean {
		if (Util.isListed(mxid, this.config.mxid.blacklist)) {
			return false;
		}
		if (Util.isListed(mxid, this.config.mxid.whitelist)) {
			return true;
		}
		return false;
	}

	/*
	 * Handles an incoming email
	 */
	public async handleIncomingEmail(email: IEmailParseResult): Promise<void> {
		log.info("New email to handle!");
		// try to fetch the room we send the email to from the db
		const threadId = await this.store.getThreadIdForMessageId(email.inReplyTo);
		if (threadId !== null) {
			if (await this.store.getThreadRoomId(threadId)) {
				await this.handleIncomingThreadEmail(email, threadId);
				return;
			}
		}

		const routes = await this.store.getEmailRoutes(email.to.email);
		let rejectEmail = this.config.email.threadRepliesOnly;
		if (rejectEmail) {
			for (const r of routes) {
				if (r.createThreadRoom) {
					rejectEmail = false;
					break;
				}
			}
		}

		if (rejectEmail) {
			// okay, this email isn't in a thread and we configured to only allow threads
			// so let's send a reply stating this
			log.info("Not a thread email, replying with rejection....");
			const sendEmail = {
				subject: `[Rejected] ${email.subject}`,
				from: email.to,
				to: email.from,
				body: await this.email.applyTemplate({email: email.to.email}, "noThreadPlain"),
				html: await this.email.applyTemplate({email: email.to.email}, "noThreadHtml"),
				reply: email.messageId,
			} as ISendEmail;
			await this.email.sendMail(sendEmail);
			return;
		}

		if (routes.length === 0) {
			const recipient = this.getMxidFromEmail(email.to.email);
			if (!recipient) {
				log.warn("Recipient email is not prefixed correctly, dropping");
				return;
			}
			routes.push({
				userId: recipient,
				createThreadRoom: false,
			});
		}

		for (const r of routes) {
			log.info(`Sending to recipient ${r.userId}`);
			await this.processIncomingEmail(email, r.userId, r.createThreadRoom);
		}
	}

	public async processIncomingEmail(
		email: IEmailParseResult,
		recipient: string,
		createThreadRoom: boolean,
	): Promise<void> {
		if (!this.canMxidUseBridge(recipient)) {
			log.warn("Recipient isn't whitelisted to use the bridge, dropping email...");
			return;
		}
		let roomId: string | null = null;
		const senderIntent = this.appservice.getIntentForUserId(this.getGhostFromEmail(email.from.email));
		if (createThreadRoom) {
			const initialState: any[] = []; // tslint:disable-line no-any
			if (this.config.createThreadRoomSendStateEvent) {
				const firstNode = email.nodes.shift();
				let stateEventData = {} as any; // tslint:disable-line no-any
				if (firstNode) {
					stateEventData = await this.prepareSendEmail(firstNode, senderIntent);
				} else {
					stateEventData = {
						body: "",
						msgtype: "m.text",
					};
				}
				stateEventData.creator = senderIntent.userId;
				stateEventData.requesting_organisation = email.from.email.split("@")[1];
				stateEventData.requested_organisation = recipient.split(":")[1];
				stateEventData.request_title = email.subject;
				stateEventData.request_id = uuid();
				stateEventData.organisation_name = email.from.email.split("@")[1];
				stateEventData.contact_id = null;
				stateEventData.contact_description = email.from.name;
				initialState.push({
					type: this.config.createThreadRoomSendStateEvent,
					content: stateEventData,
				});
				initialState.push({
					type: "m.room.history_visibility",
					content: { history_visibility: "shared" },
				});
				initialState.push({
					type: "m.room.guest_access",
					content: { guest_access: "forbidden" },
				});
			}
			roomId = await senderIntent.underlyingClient.createRoom({
				visibility: "private",
				preset: "private_chat",
				invite: [recipient],
				name: email.subject,
				initial_state: initialState,
				power_level_content_override: {
					users: {
						[senderIntent.userId]: 100,
						[recipient]: 100,
					},
				},
			});
			const threadId = await this.store.newThread(roomId, senderIntent.userId);
			await this.store.insertThreadMessageId(threadId, email.messageId);
		} else {
			roomId = await this.store.getEmailRoom(recipient);
			if (!roomId) {
				// let's create a room and invite the recipient
				roomId = await this.appservice.botIntent.underlyingClient.createRoom({
					visibility: "private",
					preset: "private_chat",
					invite: [recipient],
				});
				// and don't forget to insert the created room into the DB!
				await this.store.setEmailRoom(recipient, roomId);
			}
		}

		// ensure the ghost is registered and stuffs....
		await senderIntent.ensureRegisteredAndJoined(roomId);
		// set the display name...
		await senderIntent.underlyingClient.setDisplayName(email.from.name);

		if (createThreadRoom) {
			await this.sendEmail(email, roomId, senderIntent);
		} else {
			// and now, finally, send the email into the matrix room
			await this.sendHtmlMessage(senderIntent, roomId, `## New Email!
**Subject**: ${email.subject}`, `<h2>New Email!</h2>
<strong>Subject</strong>: ${escapeHtml(email.subject)}`);
			// let's loop over the nodes and send them bit by bit
			await this.sendEmail(email, roomId, senderIntent);
			const replyMxid = this.appservice.getAliasForSuffix(Util.str2mxid(email.messageId));
			await this.sendHtmlMessage(senderIntent, roomId, `Reply over here: [${replyMxid}](https://matrix.to/#/${replyMxid})`,
`Reply over here: <a href="https://matrix.to/#/${replyMxid}">${replyMxid}</a>`);
		}
	}

	/*
	 * Handle na incoming thread email
	 */
	private async handleIncomingThreadEmail(email: IEmailParseResult, threadId: number): Promise<void> {
		const roomId = await this.store.getThreadRoomId(threadId);
		if (!roomId) {
			log.warn("Thread entry doesn't exist, this is odd");
			return;
		}
		// we have the room, let's fetch the intent to send the message!
		const senderIntent = this.appservice.getIntentForUserId(this.getGhostFromEmail(email.from.email));
		await senderIntent.ensureRegisteredAndJoined(roomId);
		// set the display name...
		await senderIntent.underlyingClient.setDisplayName(email.from.name);

		// and finally send the email
		await this.sendEmail(email, roomId, senderIntent);

		// don't forget to insert the thread email inot the DB!
		await this.store.insertThreadMessageId(threadId, email.messageId);
	}

	private prepareSendEmailText(node: IEmailParse): IMatrixMessage {
		const ret = {
			msgtype: "m.text",
			body: node.body!,
		} as IMatrixMessage;
		if (node.formattedBody && node.formattedBody !== "") {
			ret.formatted_body = node.formattedBody;
			ret.format = "org.matrix.custom.html";
		}
		return ret;
	}

	private async prepareSendEmailFile(node: IEmailParse, senderIntent: Intent): Promise<IMatrixEventContent> {
		// we have a file message! Upload the buffer and send accordingly
		const name = node.info!.filename || "email_attachment";
		// upload the file...
		const fileMxc = await senderIntent.underlyingClient.uploadContent(
			node.buffer!,
			node.info!.mimetype,
			name,
		);
		// build the send information...
		const sendData = {
			body: name,
			info: {
				mimetype: node.info!.mimetype,
				size: node.buffer!.byteLength,
			},
			msgtype: node.msgtype,
			url: fileMxc,
		} as IMatrixEventContent;
		return sendData;
	}

	private async prepareSendEmail(
		node: IEmailParse,
		senderIntent: Intent,
	): Promise<IMatrixEventContent | IMatrixMessage> {
		if (node.type === "file") {
			return await this.prepareSendEmailFile(node, senderIntent);
		} else {
			return this.prepareSendEmailText(node);
		}
	}

	/*
	 * Sends an email to a room given an intent
	 */
	private async sendEmail(email: IEmailParseResult, roomId: string, senderIntent: Intent) {
		for (const n of email.nodes) {
			const sendData = await this.prepareSendEmail(n, senderIntent);
			await senderIntent.underlyingClient.sendMessage(roomId, sendData);
		}
	}

	/*
	 * Handle an incoming invite
	 */
	private async handleInviteEvent(roomId: string, event: IMatrixRoomEventMember): Promise<void> {
		log.info("Got invite, processing it....");
		const userId = event.state_key;
		const inviterId = event.sender;
		if (this.appservice.isNamespacedUser(inviterId)) {
			// our appservice invited, we don't want to interfer with any of this
			return;
		}
		const intent = this.appservice.getIntentForUserId(userId); // this also resolves botIntent correctly
		if (!this.canMxidUseBridge(inviterId)) {
			// we can't use the bridge, let's just reject the invite
			await intent.leaveRoom(roomId);
			return;
		}
		if (userId === this.appservice.botIntent.userId) {
			log.info("is bot user, joining room");
			// our bot user got invited
			await intent.joinRoom(roomId);
			// don't forget to set this room to the email room
			await this.store.setEmailRoom(inviterId, roomId);
			return;
		}
		// okay, we are trying to create an email thread here
		// so.....let's check if this is a valid ghost!
		const email = this.getEmailFromGhost(userId);
		if (!email) {
			log.info("invalid ghost");
			// somehow this isn't a ghost at all
			await intent.leaveRoom(roomId);
			return;
		}
		const matches = email.match(/^([^@ ]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,})$/);
		if (!matches) {
			log.info("invalid email address, leaving...");
			await intent.leaveRoom(roomId);
			return;
		}
		await intent.joinRoom(roomId);
		await this.store.newThread(roomId, userId);
		if (this.config.inviteAutoSendStateEvent) {
			// okay, we need to fetch a state and send it off!
			log.info(`Looking for state event ${this.config.inviteAutoSendStateEvent} to send...`);
			try {
				const states = await intent.underlyingClient.getRoomState(roomId);
				const sendEvent = states.find((e) => {
					return e.type === this.config.inviteAutoSendStateEvent;
				});
				if (!sendEvent.content.body) {
					log.warn("Invalid state event");
				} else {
					// we fake this being a message event
					sendEvent.type = "m.room.message";
					await this.maybeCollect(roomId, sendEvent);
				}
			} catch (err) {
				log.warn("State event not found");
				log.warn(err);
			}
		}
	}

	/*
	 * Handle a leave event
	 */
	private async handleLeaveEvent(roomId: string, event: IMatrixRoomEventMember) {
		log.info("Got leave event");
		const userId = event.state_key;
		const threadId = await this.store.getThreadId(roomId);
		if (threadId === null) {
			// this isn't a thread, nothing to do
			return;
		}
		if (this.appservice.isNamespacedUser(userId)) {
			log.info("we left / were kicked, time to unbridge");
			await this.store.deleteThread(roomId);
			return;
		}
		const ghostId = await this.store.getThreadGhost(roomId);
		if (!ghostId) {
			log.warn("Ghost doesn't exist, this is odd");
			return;
		}
		const ghostIntent = this.appservice.getIntentForUserId(ghostId);
		// okay, now we fetch the joined members and check if, apart from our ghost, there are still others
		const joinedMembers = await ghostIntent.underlyingClient.getJoinedRoomMembers(roomId);
		let othersPresent = false;
		for (const member of joinedMembers) {
			if (!this.appservice.isNamespacedUser(member) && !Util.isListed(member, this.config.mxid.ignoreForLeave)) {
				othersPresent = true;
				break;
			}
		}
		if (othersPresent) {
			log.info("Others are present, nothing to do");
			return;
		}
		log.info("Okay, we are all alone, let's unbridge this room");
		await this.store.deleteThread(roomId);
		await ghostIntent.leaveRoom(roomId);
	}

	/*
	 * Handle an incoming room event
	 */
	private async handleRoomEvent(roomId: string, event: MatrixRoomEventType): Promise<void> {
		// first check if this is a leave event
		if (event.type === "m.room.member" && ["leave", "ban"].includes(event.content.membership)) {
			await this.handleLeaveEvent(roomId, event as IMatrixRoomEventMember);
			return;
		}
		// okay, let's check if these are message event types
		const messageTypes = ["m.room.message", "m.sticker", "m.room.redaction"];
		if (messageTypes.includes(event.type)) {
			await this.handleMessageEvent(roomId, event as MatrixMessageEventType);
			return;
		}
	}

	/*
	 * A send email command was issued, process it
	 */
	private async sendMailCommand(roomId: string, event: MatrixMessageEventType): Promise<void> {
		const senderId = event.sender;
		// parse to start collecting
		log.info("Got command to send email...");
		// first we parse the arguments of the email and check if a valid email was supplied
		const args = event.content.body.substring("!sendmail ".length);
		const matches = args.match(/^([^@ ]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}) ?(.*)$/);
		const MATCH_EMAIL = 1;
		const MATCH_SUBJECT = 2;
		if (!matches) {
			// invalid email, cancle
			await this.appservice.botIntent.sendText(roomId, "ERROR: Invalid email address");
			return;
		}
		// now we fetch the to email and the subject
		const toEmail = matches[MATCH_EMAIL];
		const subject = matches[MATCH_SUBJECT];
		// start the actual message collecting
		await this.messageCollector.start(roomId, senderId);
		await this.appservice.botIntent.sendText(roomId, "Collecting messages for email...");
		// wait for the messages to be collected
		const events = await this.messageCollector.wait(roomId, senderId);
		log.info("Done collecting messages, parsing them into an email...");
		// and now parse the collected events
		const email = await this.matrixParser.parse(events);
		if (!email) {
			log.info("Email empty, doing nothing");
			await this.appservice.botIntent.sendText(roomId, "Empty email, doing nothing!");
			return;
		}
		// let's fetch the name of who is sending the mail
		const senderName = await this.getNameFromMxid(senderId, roomId);
		// and finally send the email!
		email.subject = subject;
		email.to = {
			email: toEmail,
		};
		email.from = {
			email: this.getEmailFromMxid(senderId),
		};
		if (senderName) {
			email.from.name = senderName;
		}
		log.info("Sending email...");
		const newMessageId = await this.email.sendMail(email);
		await this.appservice.botIntent.sendText(roomId, "email sent!");
	}

	/*
	 * Process if a message is in a threaded room, and if so, start collecting
	 */
	private async maybeCollect(roomId: string, event: MatrixMessageEventType): Promise<void> {
		const senderId = event.sender;
		if (this.messageCollector.isCollecting(roomId, senderId)) {
			log.verbose("Collecting message");
			this.messageCollector.collect(roomId, senderId, event);
			return;
		}
		const threadId = await this.store.getThreadId(roomId);
		if (threadId === null) {
			// okay, this actually isn't a thread room, just drop it
			return;
		}
		log.info("New message to start collecting in a thread room!");

		// first get some basic information
		const ghostId = await this.store.getThreadGhost(roomId);
		if (!ghostId) {
			log.warn("Apparently we aren't a thread room after all...");
			return;
		}
		const ghostIntent = this.appservice.getIntentForUserId(ghostId);
		const toEmail = this.getEmailFromGhost(ghostId);
		if (!toEmail) {
			log.warn("Invalid email address, doing nothing...");
			return;
		}

		// start the actual message collecting
		await this.messageCollector.start(roomId, senderId);
		// we still need to collect this message...
		this.messageCollector.collect(roomId, senderId, event);

		// we might as well fetch user name and subject and stuffs while collecting messages
		const toName = await this.getNameFromMxid(ghostId, roomId, ghostIntent.underlyingClient);
		const fromEmail = this.getEmailFromMxid(senderId);
		const fromName = await this.getNameFromMxid(senderId, roomId, ghostIntent.underlyingClient);
		let subject = await this.getRoomNameFromMxid(roomId, ghostIntent.underlyingClient);
		const inReplyTo = await this.store.getLatestThreadMessageId(threadId);

		// and finally wait for being done collecting
		const events = await this.messageCollector.wait(roomId, senderId);
		log.info("Done collecting messages, parsing them into an email...");
		const email = await this.matrixParser.parse(events);
		if (!email) {
			log.info("Email empty, doing nothing");
			return;
		}

		// okay, time to build the email
		if (!subject) {
			subject = "";
		}
		if (inReplyTo) {
			subject = `Re: ${subject}`;
			email.reply = inReplyTo;
		}
		email.subject = subject;
		email.to = { email: toEmail };
		if (toName) {
			email.to.name = toName;
		}
		email.from = { email: fromEmail };
		if (fromName) {
			email.from.name = fromName;
		}
		log.info("Sending email...");
		const newMessageId = await this.email.sendMail(email);
		if (newMessageId) {
			await this.store.insertThreadMessageId(threadId, newMessageId);
		}

		// finally, at last, send the read indicator
		const lastEventId = events[events.length - 1].event_id;
		await ghostIntent.underlyingClient.sendReadReceipt(roomId, lastEventId);
	}

	private async handleMessageEvent(roomId: string, event: MatrixMessageEventType): Promise<void> {
		const senderId = event.sender;
		if (this.appservice.isNamespacedUser(senderId)) {
			return; // we don't handle anything sent by ourself
		}
		log.info("Got a new matrix event!");

		const emailRoom = await this.store.getEmailRoom(senderId);

		if (emailRoom === roomId && event.type === "m.room.message" && event.content.body.startsWith("!sendmail ")) {
			// sendmail only works in email room
			if (!this.canMxidUseBridge(senderId)) {
				// person can't send emails (at least via sendmail command)
				// just silently drop them
				return;
			}
			if (this.messageCollector.isCollecting(roomId, senderId)) {
				// already collecting error message
				this.messageCollector.stop(roomId, senderId);
			}
			await this.sendMailCommand(roomId, event);
			return;
		}

		await this.maybeCollect(roomId, event);
	}

	/*
	 * This handles a room query
	 */
	// tslint:disable-next-line no-any
	private async handleRoomQuery(alias: string, createRoom: any) {
		log.info(`Got room query for alias ${alias}`);
		// we always deny room creation and later on create it outselves, if needed
		await createRoom(false);
		const suffix = this.appservice.getSuffixForAlias(alias);
		if (!suffix) {
			// not proper alias
			log.verbose("Improper suffix, doing nothing");
			return;
		}
		const messageId = Util.mxid2str(suffix);
		// now that we have the message ID, attempt to fetch the associated email
		const email = await this.email.getMailByMessageId(messageId);
		if (!email) {
			log.verbose("email not found, doing nothing");
			return;
		}
		const recipient = this.getMxidFromEmail(email.to.email);
		if (!recipient) {
			log.verbose("Recipient email is not prefixed correctly, doing nothing");
			return;
		}
		// fetch the intent to create the room
		const ghostId = this.getGhostFromEmail(email.from.email);
		const senderIntent = this.appservice.getIntentForUserId(ghostId);
		// set the display name...
		await senderIntent.underlyingClient.setDisplayName(email.from.name);

		// create the room
		const roomId = await senderIntent.underlyingClient.createRoom({
			visibility: "private",
			invite: [recipient],
			room_alias_name: this.appservice.getAliasLocalpartForSuffix(suffix),
			name: email.subject || undefined,
		});
		await senderIntent.ensureRegisteredAndJoined(roomId);
		// insert the room as a new thread
		const threadId = await this.store.newThread(roomId, ghostId);
		// insert the message into the thread
		await this.store.insertThreadMessageId(threadId, email.messageId);

		// and finally, let's just send the email into that room again
		await this.sendEmail(email, roomId, senderIntent);
	}

	private async sendHtmlMessage(intent: Intent, roomId: string, body: string, html: string): Promise<void> {
		await intent.underlyingClient.sendMessage(roomId, {
			msgtype: "m.text",
			body,
			formatted_body: html,
			format: "org.matrix.custom.html",
		});
	}
}
