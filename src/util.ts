/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import * as fileType from "file-type";
import * as request from "request-promise";

export class Util {
	// tslint:disable-next-line no-any
	public static async DownloadFile(url: string, options: any = {}): Promise<Buffer> {
		if (!options.method) {
			options.method = "GET";
		}
		options.url = url;
		options.encoding = null;
		return await request(options);
	}

	public static GetMimeType(buffer: Buffer): string | undefined {
		const typeResult = fileType(buffer);
		if (!typeResult) {
			return undefined;
		}
		return typeResult.mime;
	}

	public static str2mxid(a: string): string {
		// tslint:disable:no-magic-numbers
		const buf = Buffer.from(a);
		let encoded = "";
		for (const b of buf) {
			if (b === 0x5F) {
				// underscore
				encoded += "__";
			} else if ((b >= 0x61 && b <= 0x7A) || (b >= 0x30 && b <= 0x39)) {
				// [a-z0-9]
				encoded += String.fromCharCode(b);
			} else if (b >= 0x41 && b <= 0x5A) {
				encoded += "_" + String.fromCharCode(b + 0x20);
			} else if (b < 16) {
				encoded += "=0" + b.toString(16);
			} else {
				encoded += "=" + b.toString(16);
			}
		}
		return encoded;
		// tslint:enable:no-magic-numbers
	}

	public static mxid2str(b: string): string {
		// tslint:disable:no-magic-numbers
		const decoded = Buffer.alloc(b.length);
		let j = 0;
		for (let i = 0; i < b.length; i++) {
			const char = b[i];
			if (char === "_") {
				i++;
				if (b[i] === "_") {
					decoded[j] = 0x5F;
				} else {
					decoded[j] = b[i].charCodeAt(0) - 0x20;
				}
			} else if (char === "=") {
				i++;
				decoded[j] = parseInt(b[i] + b[i + 1], 16);
				i++;
			} else {
				decoded[j] = b[i].charCodeAt(0);
			}
			j++;
		}
		return decoded.toString("utf8", 0, j);
		// tslint:enable:no-magic-numbers
	}

	public static async sleep(timeout: number): Promise<void> {
		return new Promise((resolve, reject) => {
			setTimeout(resolve, timeout);
		});
	}

	public static isListed(target: string, list: string[]): boolean {
		for (const l of list) {
			if (target.match(l)) {
				return true;
			}
		}
		return false;
	}
}
