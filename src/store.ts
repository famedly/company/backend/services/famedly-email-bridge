/*
Copyright 2017 - 2019 matrix-appservice-discord

Modified for famedly-email-bridge
Copyright 2019 Famedly

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as fs from "fs";
import { IDbSchema } from "./db/schema/dbschema";
import { Log } from "./log";
import { EmailDatabaseConfig } from "./config";
import { Postgres } from "./db/postgres";
import { IDatabaseConnector } from "./db/connector";

const log = new Log("Store");

export interface IEmailRouteResult {
	userId: string;
	createThreadRoom: boolean;
}

export const CURRENT_SCHEMA = 4;
/**
 * Stores data for specific users and data not specific to rooms.
 */
export class Store {
	public db: IDatabaseConnector;

	constructor(
		private config: EmailDatabaseConfig,
	) { }

	/**
	 * Checks the database has all the tables needed.
	 */
	public async init(overrideSchema: number = 0): Promise<void> {
		log.info("Starting DB Init");
		await this.openDatabase();
		let version = await this.getSchemaVersion();
		const targetSchema = overrideSchema || CURRENT_SCHEMA;
		log.info(`Database schema version is ${version}, latest version is ${targetSchema}`);
		while (version < targetSchema) {
			version++;
			const schemaClass = require(`./db/schema/v${version}.js`).Schema;
			const schema = (new schemaClass() as IDbSchema);
			log.info(`Updating database to v${version}, "${schema.description}"`);
			try {
				await schema.run(this);
				log.info("Updated database to version ", version);
			} catch (ex) {
				log.error("Couldn't update database to schema ", version);
				log.error(ex);
				log.info("Rolling back to version ", version - 1);
				try {
					await schema.rollBack(this);
				} catch (ex) {
					log.error(ex);
					throw Error("Failure to update to latest schema. And failed to rollback.");
				}
				throw Error("Failure to update to latest schema.");
			}
			await this.setSchemaVersion(version);
		}
		log.info("Updated database to the latest schema");
	}

	public async close() {
		await this.db.Close();
	}

	public async createTable(statement: string, tablename: string): Promise<void|Error> {
		try {
			await this.db.Exec(statement);
			log.info("Created table", tablename);
		} catch (err) {
			throw new Error(`Error creating '${tablename}': ${err}`);
		}
	}

	public async getEmailRoom(userId: string): Promise<string | null> {
		const res = await this.db.Get("SELECT room_id FROM email_room WHERE user_id = $id", {
			id: userId,
		});
		if (!res) {
			return null;
		}
		return res.room_id as string;
	}

	public async setEmailRoom(userId: string, roomId: string): Promise<void> {
		await this.db.Run("DELETE FROM email_room WHERE user_id = $u", {
			u: userId,
		});
		await this.db.Run("INSERT INTO email_room (user_id, room_id) VALUES ($u, $r)", {
			u: userId,
			r: roomId,
		});
	}

	public async getThreadId(roomId: string): Promise<number | null> {
		const res = await this.db.Get("SELECT id FROM thread_room WHERE room_id = $id", {
			id: roomId,
		});
		if (!res) {
			return null;
		}
		return res.id as number;
	}

	public async getThreadRoomId(threadId: number): Promise<string | null> {
		const res = await this.db.Get("SELECT room_id FROM thread_room WHERE id = $id", {
			id: threadId,
		});
		if (!res) {
			return null;
		}
		return res.room_id as string;
	}

	public async getThreadGhost(roomId: string): Promise<string | null> {
		const res = await this.db.Get("SELECT ghost_id FROM thread_room WHERE room_id = $id", {
			id: roomId,
		});
		if (!res) {
			return null;
		}
		return res.ghost_id as string;
	}

	public async newThread(roomId: string, ghostId: string): Promise<number> {
		return await this.db.Run("INSERT INTO thread_room (room_id, ghost_id) VALUES ($r, $g)", {
			r: roomId,
			g: ghostId,
		}, "id") as number;
	}

	public async deleteThread(roomId: string): Promise<void> {
		const threadId = await this.getThreadId(roomId);
		if (threadId !== null) {
			await this.db.Run("DELETE FROM thread_message WHERE thread_id = $id", {
				id: threadId,
			});
		}
		await this.db.Run("DELETE FROM thread_room WHERE room_id = $r", {
			r: roomId,
		});
	}

	public async getLatestThreadMessageId(threadId: number): Promise<string | null> {
		const res = await this.db.Get(
			"SELECT message_id FROM thread_message WHERE thread_id = $id ORDER BY id DESC LIMIT 1", {
			id: threadId,
		});
		if (!res) {
			return null;
		}
		return res.message_id as string;
	}

	public async insertThreadMessageId(threadId: number, messageId: string): Promise<void> {
		await this.db.Run("INSERT INTO thread_message (thread_id, message_id) VALUES ($tid, $mid)", {
			tid: threadId,
			mid: messageId,
		});
	}

	public async getThreadIdForMessageId(messageId: string | null): Promise<number | null> {
		if (!messageId) {
			return null;
		}
		const res = await this.db.Get("SELECT thread_id FROM thread_message WHERE message_id = $m", {
			m: messageId,
		});
		if (!res) {
			return null;
		}
		return res.thread_id as number;
	}

	public async getEmailRoutes(email: string): Promise<IEmailRouteResult[]> {
		const rows = await this.db.All("SELECT * FROM email_route WHERE email = $email", {email});
		if (!rows) {
			return [];
		}
		const result: IEmailRouteResult[] = [];
		for (const r of rows) {
			result.push({
				userId: r.user_id as string,
				createThreadRoom: r.create_thread_room as boolean,
			});
		}
		return result;
	}

	private async getSchemaVersion( ): Promise<number> {
		log.debug("_get_schema_version");
		let version = 0;
		try {
			const versionReply = await this.db.Get(`SELECT version FROM schema`);
			version = versionReply!.version as number;
		} catch (er) {
			log.warn("Couldn't fetch schema version, defaulting to 0");
		}
		return version;
	}

	private async setSchemaVersion(ver: number): Promise<void> {
		log.debug("_set_schema_version => ", ver);
		await this.db.Run(
			`
			UPDATE schema
			SET version = $ver
			`, {ver},
		);
	}

	private async openDatabase(): Promise<void|Error> {
		log.info("connecting to postgres");
		this.db = new Postgres(this.config.connString);
		try {
			this.db.Open();
		} catch (ex) {
			log.error("Error opening database:", ex);
			throw new Error("Couldn't open database. The appservice won't be able to continue.");
		}
	}
}
