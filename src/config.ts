/*
Copyright (C) 2019  Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

const THREE_MIN = 3 * 60 * 1000; // tslint:disable-line no-magic-numbers

export class EmailConfig {
	public bridge: EmailBridgeConfig = new EmailBridgeConfig();
	public database: EmailDatabaseConfig = new EmailDatabaseConfig();
	public logging: EmailLoggingConfig = new EmailLoggingConfig();
	public email: EmailEmailConfig = new EmailEmailConfig();
	public mxid: EmailMxidConfig = new EmailMxidConfig();
	public messageCollectingTimeout: number = THREE_MIN;
	public inviteAutoSendStateEvent: string;
	public createThreadRoomSendStateEvent: string;

	// tslint:disable-next-line no-any
	public applyConfig(newConfig: {[key: string]: any}, layer: {[key: string]: any} = this) {
		for (const key in newConfig) {
			if (newConfig.hasOwnProperty(key)) {
				if (layer[key] instanceof Object && !(layer[key] instanceof Array)) {
					this.applyConfig(newConfig[key], layer[key]);
				} else {
					layer[key] = newConfig[key];
				}
			}
		}
	}
}

class EmailBridgeConfig {
	public bindAddress: string = "localhost";
	public port: number;
	public domain: string;
	public homeserverUrl: string;
}

export class EmailDatabaseConfig {
	public connString: string = "";
}

export class EmailLoggingConfig {
	public console: string = "info";
	public lineDateFormat: string = "MMM-D HH:mm:ss.SSS";
	public files: LoggingFile[] = [];
}

export class LoggingFile {
	public file: string;
	public level: string = "info";
	public maxFiles: string = "14d";
	public maxSize: string|number = "50m";
	public datePattern: string = "YYYY-MM-DD";
	public enabled: string[] = [];
	public disabled: string[] = [];
}

export class EmailEmailConfig {
	public imap: EmailImapConfig = new EmailImapConfig();
	public smtp: EmailSmtpConfig = new EmailSmtpConfig();
	public prefix: string;
	public domain: string;
	public inbox: string = "INBOX";
	public templates: EmailTemplatesConfig = new EmailTemplatesConfig();
	public threadRepliesOnly: boolean = false;
}

class EmailImapConfig {
	public user: string;
	public password: string;
	public host: string;
	public port: number = 993;
	public tls: boolean = true;
}

class EmailSmtpConfig {
	public user: string;
	public password: string;
	public host: string;
	public port: number = 993;
	public tls: boolean = true;
}

class EmailTemplatesConfig {
	public plain: string = "templates/plain.txt";
	public html: string = "templates/html.txt";
	public noThreadPlain: string = "templates/no_thread_plain.txt";
	public noThreadHtml: string = "templates/no_thread_html.txt";
}

class EmailMxidConfig {
	public whitelist: string[] = [];
	public blacklist: string[] = [];
	public ignoreForLeave: string[] = [];
}
