import { Util } from "./util";
import { Lock } from "./structures/lock";
import { MatrixMessageEventType } from "./matrixtypes";

const CREATELOCK_TIMEOUT = 1000; // tslint:disable-line no-magic-numbers

interface ICollect {
	userId: string;
	events: MatrixMessageEventType[];
}

export class MessageCollector {
	private readonly collected: Map<string, ICollect>;
	// this lock is responsible as the timeout for how long it collects messages
	private readonly lock: Lock<string>;
	// this lock is needed to lock collecting creation briefly when it is released
	private readonly createLock: Lock<string>;
	public constructor(timeout: number) {
		this.collected = new Map();
		this.lock = new Lock(timeout);
		this.createLock = new Lock(CREATELOCK_TIMEOUT);
	}

	/*
	 * Check if we are collecting a specific roomId & userId atm
	 * If the roomId matches but not the userId we stop collecting
	 */
	public isCollecting(roomId: string, userId: string): boolean {
		// first check if we are collecting anything in this room
		const entry = this.collected.get(roomId);
		if (!entry) {
			return false;
		}
		// it is just our user that is collecting, great!
		if (entry.userId === userId) {
			return true;
		}
		// okay, a different user is collecting than we, so let's make them stop collecting...
		this.stop(roomId, userId);
		return false;
	}

	/*
	 * Collect an event
	 */
	public collect(roomId: string, userId: string, event: MatrixMessageEventType): void {
		// if we are the current room and userId collecting then....let's collect!
		const entry = this.collected.get(roomId);
		if (!entry || entry.userId !== userId) {
			return;
		}
		entry.events.push(event);
		this.collected.set(roomId, entry);
	}

	/*
	 * Start collecting a specific roomId / userId
	 * This does **NOT** check if something is collecting already,
	 * please do so manually beforehand!
	 */
	public async start(roomId: string, userId: string): Promise<void> {
		// first we wait for the creation lock to expire
		await this.createLock.wait(roomId);
		// then we start collecting! :D
		this.collected.set(roomId, {
			userId,
			events: [],
		});
		this.lock.set(roomId);
	}

	/*
	 * Manually stop collecting
	 * This does **NOT** check if something is collecting already,
	 * please do so manually beforehand!
	 */
	public stop(roomId: string, userId: string): void {
		// first set a creation lock
		this.createLock.set(roomId);
		// and then release the collection lock
		this.lock.release(roomId);
	}

	/*
	 * Wait for the collection to be done and return all the collected
	 * events
	 */
	public async wait(roomId: string, userId: string): Promise<MatrixMessageEventType[]> {
		// first we wait for the collection to be done
		await this.lock.wait(roomId);

		// next we fetch the collected entries
		const entry = this.collected.get(roomId);
		if (!entry) {
			return [];
		}
		// delete the entries
		this.collected.delete(roomId);
		// and release the create lock! We are free to create a new collection now
		this.createLock.release(roomId);
		return entry.events;
	}
}
