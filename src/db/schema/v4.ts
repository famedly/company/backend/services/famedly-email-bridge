/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { IDbSchema } from "./dbschema";
import { Store } from "../../store";

export class Schema implements IDbSchema {
	public description = "email_route";
	public async run(store: Store) {
		await store.createTable(`
			CREATE TABLE email_route(
				id SERIAL PRIMARY KEY,
				email VARCHAR(255) NOT NULL,
				user_id VARCHAR(255) NOT NULL,
				create_thread_room BOOLEAN DEFAULT FALSE
			);`, "email_route");
	}

	public async rollBack(store: Store) {
		await store.db.Exec("DROP TABLE IF EXISTS email_route");
	}
}
