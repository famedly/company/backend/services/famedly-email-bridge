/*
Copyright (C) 2019 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { IDbSchema } from "./dbschema";
import { Store } from "../../store";

export class Schema implements IDbSchema {
	public description = "add ghost_id to thread_room";
	public async run(store: Store) {
		await store.db.Exec("ALTER TABLE thread_room ADD COLUMN ghost_id VARCHAR(255) NOT NULL DEFAULT ''");
		await store.db.Exec("ALTER TABLE thread_room ALTER COLUMN ghost_id DROP DEFAULT");
	}

	public async rollBack(store: Store) {
		await store.db.Exec("ALTER TABLE thread_room DROP COLUMN ghost_id");
	}
}
