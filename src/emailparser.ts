/*
Copyright (C) 2019  Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import * as mimemessage from "mimemessage";
import { Util } from "./util";
import * as sanitizeHtml from "sanitize-html";

interface IEmailParseFileInfo {
	filename?: string;
	mimetype: string;
}

interface IEmailPreparse {
	type: "text" | "html" | "file";
	body?: string;
	buffer?: Buffer;
	info?: IEmailParseFileInfo;
}

export interface IEmailParse {
	type: "text" | "file";
	msgtype: string;
	body?: string;
	formattedBody?: string;
	buffer?: Buffer;
	info?: IEmailParseFileInfo;
}

interface IEmailParseEmail {
	name: string;
	email: string;
}

export interface IEmailParseResult {
	messageId: string;
	inReplyTo: string | null;
	references: string | null;
	to: IEmailParseEmail;
	from: IEmailParseEmail;
	subject: string | null;
	nodes: IEmailParse[];
}

// from https://github.com/matrix-org/matrix-react-sdk/blob/develop/src/HtmlUtils.js
const sanitizeHtmlParams = {
	allowedTags: [
		"font", // custom to matrix for IRC-style font coloring
		"del", // for markdown
		"h1", "h2", "h3", "h4", "h5", "h6", "blockquote", "p", "a", "ul", "ol", "sup", "sub",
		"nl", "li", "b", "i", "u", "strong", "em", "strike", "code", "hr", "br", "div",
		"table", "thead", "caption", "tbody", "tr", "th", "td", "pre", "span", "img",
	],
	allowedAttributes: {
		// custom ones first:
		font: ["color", "data-mx-bg-color", "data-mx-color", "style"], // custom to matrix
		span: ["data-mx-bg-color", "data-mx-color", "data-mx-spoiler", "style"], // custom to matrix
		a: ["href", "name", "target", "rel"], // remote target: custom to matrix
		img: ["src", "width", "height", "alt", "title"],
		ol: ["start"],
		code: ["class"], // We don't actually allow all classes, we filter them in transformTags
	},
	// Lots of these won't come up by default because we don't allow them
	selfClosing: ["img", "br", "hr", "area", "base", "basefont", "input", "link", "meta"],
	// URL schemes we permit
	allowedSchemes: ["http", "https", "ftp", "mailto", "magnet"],

	allowProtocolRelative: false,
};

export class EmailParser {
	public parse(emailBody: string): IEmailParseResult {
		const parsed = mimemessage.parse(emailBody);
		const result = {
			messageId: this.normalizeMessageId(parsed.header("Message-ID")),
			inReplyTo: this.normalizeMessageId(parsed.header("In-Reply-To")),
			references: this.normalizeMessageId(parsed.header("References")),
			to: this.normalizeEmailHeader(parsed.header("To")),
			from: this.normalizeEmailHeader(parsed.header("From")),
			subject: parsed.header("Subject") || null,
			nodes: [],
		} as IEmailParseResult;

		// first we parse entity by entity, after that we format that into matrix-style events
		const preparse = this._parse(parsed.body);
		// now we need to postparse this thing
		const textMessage = {
			type: "text",
			msgtype: "m.text",
			body: "",
			formattedBody: "",
		} as IEmailParse;

		for (const node of preparse) {
			switch (node.type) {
				case "text":
					// we trim the excess whitespace off (it is a lot)
					textMessage.body! += node.body!.trim();
					break;
				case "html": {
					// this is a tad more complex, as we need to only get the contentds of the body tag
					// we are going to make it easy for us and strip everything before <body> and after </body>
					let html = node.body!;
					html = html.substring(html.indexOf("<body>") + "<body>".length);
					html = html.substring(0, html.indexOf("</body>"));
					html = html.trim();
					html = this.sanitizeHtml(html);
					textMessage.formattedBody! += html;
					break;
				}
				case "file": {
					// let's first determine the matrix msg type based off of the mimetype
					let type = "file";
					if (node.info) {
						type = node.info.mimetype.split("/")[0];
					}
					let msgtype = {
						audio: "m.audio",
						image: "m.image",
						video: "m.video",
					}[type];
					if (!msgtype) {
						msgtype = "m.file";
					}

					// now we push the file node
					result.nodes.push({
						type: "file",
						msgtype,
						buffer: node.buffer,
						info: node.info,
					});
					break;
				}
			}
		}
		// we musn't forget to insert our text node at first entry, else it will be sent after the attachments
		// or completely forgotten, only if it has content, though
		if (textMessage.body !== "") {
			result.nodes.unshift(textMessage);
		}
		return result;
	}

	/*
	 * Normalizes a message ID to its email
	 */
	public normalizeMessageId(id?: string): string | null {
		if (!id) {
			return null;
		}
		const matches = id.match(/<([^<>]+)>/);
		return matches ? matches[1] : null;
	}

	/*
	 * This method parses an entity and appends it to the messages array, returning it
	 */
	private _parse(entity: mimemessage.Entity, messages: IEmailPreparse[] = []) {
		// if we actually passed an array of entities we must parse each individually
		if (Array.isArray(entity)) {
			for (const e of entity) {
				this._parse(e, messages);
			}
			return messages;
		}

		// if our entity is just a string it is plain text!
		if (typeof entity === "string") {
			messages.push({
				type: "text",
				body: entity,
			});
			return messages;
		}

		const contentDisposition = entity.contentDisposition();
		const type = entity.contentType();
		// first test if we have an attachment
		if (contentDisposition && contentDisposition.value.includes("attachment")) {
			// the buffer is a string in binary format, so let's parse this correctly
			const buffer = Buffer.from(entity.body, "binary");

			// first we try to get the mime type from the buffer, if that fails we use the one specified
			// that way we validate mimetype correctness while still making things like text/plain possible
			const mimetype = Util.GetMimeType(buffer) || type.fulltype || "application/octet-stream";

			const filename = contentDisposition.params.filename || "email_attachment";
			messages.push({
				type: "file",
				buffer,
				info: {
					filename,
					mimetype,
				},
			});
			return messages;
		}

		// if we actually have a multipart entity we need to parse all the children
		if (entity.isMultiPart()) {
			for (const e of entity.body) {
				this._parse(e, messages);
			}
			return messages;
		}

		// here we do plain and html separately, later on they are combind
		if (type.type === "text") {
			switch (type.subtype) {
				case "plain":
					messages.push({
						type: "text",
						body: entity.body,
					});
					return messages;
				case "html":
					messages.push({
						type: "html",
						body: entity.body,
					});
					return messages;
			}
		}

		// okay, this isn't an entity we handle, let's just return
		return messages;
	}

	/*
	 * Normalizes an email header (To and From)
	 */
	private normalizeEmailHeader(header?: string): IEmailParseEmail | null {
		if (!header) {
			return null;
		}

		const matches = header.match(/^(?:(.*) <([^<>]+)>|([^<>]+))$/);
		const MATCH_NAME = 1;
		const MATCH_EMAIL = 2;
		const MATCH_EMAIL_ONLY = 3;
		if (!matches) {
			return null;
		}

		const result = {
			name: matches[MATCH_EMAIL_ONLY],
			email: matches[MATCH_EMAIL_ONLY],
		} as IEmailParseEmail;
		if (matches[MATCH_NAME]) {
			result.name = matches[MATCH_NAME];
			result.email = matches[MATCH_EMAIL];
		}
		return result;
	}

	/*
	 * Sanitize the HTML to be matrix-sane
	 */
	private sanitizeHtml(html: string): string {
		return sanitizeHtml(html, sanitizeHtmlParams);
	}
}
