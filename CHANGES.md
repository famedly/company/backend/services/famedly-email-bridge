# famedly-email-bridge 0.1.0
Initial release, including:
 - Receiving emails
 - Parsing emails to matrix messages
 - Sending emails
 - Parsing matrix messages to emails
 - Reply to emails in threads
 - Create new threads
 - Route emails to correct threads
