# famedly-email-bridge
**famedly-email-bridge** is an email bridge for matrix. A postgres instance as database is required.

## Features
 - Receive emails
   - plain emails
   - html emails
   - attachments
 - Send emails
   - message collecting of multiple messages
   - plain messages
   - rich formatted messages
   - images, audio, videos, files
   - edits
   - redactions
 - Threads
   - Start an email thread
   - Reply to an email, creating an email thread

## Sending an email
To send in email you type into the created control room with the bot `!sendmail <email> <subject>`. Subject is optional.
After that, message collecting starts. For a given time (default: 3min) you can send messages, edit them and redact them
as you wish in matrix. Once the time is up all those messages are bundled into an email and sent out.

## Email Threads
To create an email thread simply create an email ghost to a new room. Once that ghost joined automatically type the messages you want to send to it. Consecutive replies to it will be routed to this room!

As for replying to existing emails, simply click the channel pill presented below the email. While your client will say that the room doesn't exist, soon the ghost will invite you to a room where you can then simply send your reply message.

## Installation
The bridge requires an IMAP server and an SMTP server to be configured and running. All received emails of the set prefix
have to land in the same inbox. If you are using postfix this can be done e.g. with `virtual_alias_maps = regexp:/etc/postfix/virtual`
and then an entry `/matrix_.*@yourserver\.com/ matrix-email-user` in that file. The user of the SMTP server also has to be
able to send from all email addresses with that prefix.

First you have to clone and install the repo:
```bash
git clone git@gitlab.com:famedly/bridges/famedly-email-bridge.git
cd famedly-email-bridge
npm install
```

Next, copy the `config.sample.yaml` to `config.yaml` and edit it to your liking.

After that, you have to generate a registration file:
```bash
npm run start -- [-c config.yaml] [-f email-registration.yaml] -r
```

Register the registration file in synapse `homeserver.yaml` and restart it. Finally you can start the bridge:
```bash
npm run start -- [-c config.yaml] [-f email-registration.yaml]
```
